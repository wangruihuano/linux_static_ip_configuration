# linux静态ip配置

#### 项目介绍
静态ip配置虽然不是什么技术活,但网上教程都较为陈旧,系统版本参差不齐,所以在这里记录以下,便于使用.

#### 使用说明

1. 目前测试的操作系统是CentOS7,
2. 下载本目录中的[ifcfg-xxx配置文件](./ifcfg-xxx)到系统的 `/etc/sysconfig/network-scripts/` 目录下.
3. 根据你自己的情况修改配置文件名称及配置文件中的内容.
4. 重启网络(`systemctl restart network`或者 `service network restart`)或系统.
5. 查看ip可以使用 `ip addr`.